sap.ui.define([
	"sap/ui/core/mvc/Controller",
	'sap/ui/model/json/JSONModel'
], function (Controller, JSONModel) {
	"use strict";

	return Controller.extend("LAB.GmapUI5.controller.View1", {
		onInit: function () {
			var Business = [{
				"id": "B0001",
				"raisonSociale": "Stark Industries",
				"secteurActivite": "Armement",
				"CA": "64250654",
				"email": "tony.stark@starkindustries.com",
				"phone": "09 13 55 41 80",
				"address": "10880 Malibu Point",
				"postalCode": "90265",
				"ville": "Malibu",
				"pays": "États-Unis",
				"latitude": "34.0314469",
				"longitude": "-118.6822872",
				"volumeAchatUnite": "600 000",
				"volumeAchatCumulAnnee": "12 000 000",
				"importancePartner": "Gold",
				"commercial": "Pierre GREGORUTTI",
				"acheteur": "Pascal-Pierre COULON",
				"volumeAchatCumulInterval": [{
					"date": "12/01/2019",
					"montant": "60000"
				}, {
					"date": "12/02/2019",
					"montant": "10000"
				}, {
					"date": "12/03/2019",
					"montant": "80000"
				}, {
					"date": "12/04/2019",
					"montant": "80000"
				}, {
					"date": "12/05/2019",
					"montant": "80000"
				}, {
					"date": "12/06/2019",
					"montant": "80000"
				}, {
					"date": "12/07/2019",
					"montant": "80000"
				}, {
					"date": "12/08/2019",
					"montant": "80000"
				}, {
					"date": "12/09/2019",
					"montant": "80000"
				}, {
					"date": "12/10/2019",
					"montant": "80000"
				}, {
					"date": "12/11/2019",
					"montant": "80000"
				}, {
					"date": "12/12/2019",
					"montant": "80000"
				}, {
					"date": "12/13/2019",
					"montant": "80000"
				}, {
					"date": "12/14/2019",
					"montant": "80000"
				}, {
					"date": "12/15/2019",
					"montant": "110000"
				}, {
					"date": "12/16/2019",
					"montant": "110000"
				}, {
					"date": "12/17/2019",
					"montant": "110000"
				}, {
					"date": "12/18/2019",
					"montant": "90000"
				}, {
					"date": "12/19/2019",
					"montant": "60000"
				}, {
					"date": "12/20/2019",
					"montant": "25000"
				}, {
					"date": "12/21/2019",
					"montant": "35000"
				}, {
					"date": "12/22/2019",
					"montant": "75000"
				}, {
					"date": "12/23/2019",
					"montant": "78000"
				}, {
					"date": "12/24/2019",
					"montant": "65000"
				}, {
					"date": "12/25/2019",
					"montant": "34000"
				}, {
					"date": "12/26/2019",
					"montant": "19000"
				}, {
					"date": "12/27/2019",
					"montant": "68000"
				}, {
					"date": "12/28/2019",
					"montant": "99000"
				}, {
					"date": "12/29/2019",
					"montant": "160000"
				}, {
					"date": "12/30/2019",
					"montant": "9000"
				}, {
					"date": "12/31/2019",
					"montant": "30000"
				}],
				"wharehouse": [{
					"id": "WH0001",
					"address": "2323 Diamond Dr",
					"postalCode": "87544",
					"ville": "Los Alamos",
					"email": "wharehouseLosAlamos@starkindustries.com",
					"phone": "05 49 63 45 85",
					"latitude": "35.678807",
					"longitude": "-106.726932"
				}, {
					"id": "WH0002",
					"address": "Comté de Clackamas",
					"postalCode": "97005",
					"ville": "Oregon",
					"email": "wharehouseClackamas@starkindustries.com",
					"phone": "02 54 69 85 25",
					"latitude": "45.255607",
					"longitude": "-122.269478"
				}]
			}, {
				"id": "B0002",
				"raisonSociale": "Marty Mcfly Voyages",
				"secteurActivite": "Divertissement",
				"CA": "10654980",
				"email": "marty.mcfly@mmvoyages.com",
				"phone": "07 60 45 35 10",
				"address": "9303 Lyon Drive",
				"postalCode": "95420",
				"ville": "Hill Valley",
				"pays": "États-Unis",
				"latitude": "37.644235",
				"longitude": "-118.973611",
				"volumeAchatUnite": "2 000",
				"volumeAchatCumulAnnee": "2 000 000",
				"importancePartner": "Silver",
				"commercial": "Julien LUCET",
				"acheteur": "Pascal-Pierre COULON",
				"volumeAchatCumulInterval": [{
					"date": "12/01/2019",
					"montant": "8000"
				}, {
					"date": "12/02/2019",
					"montant": "4000"
				}, {
					"date": "12/03/2019",
					"montant": "3000"
				}, {
					"date": "12/04/2019",
					"montant": "12000"
				}, {
					"date": "12/052019",
					"montant": "21000"
				}, {
					"date": "12/06/2019",
					"montant": "6500"
				}, {
					"date": "12/07/2019",
					"montant": "3200"
				}, {
					"date": "12/08/2019",
					"montant": "14000"
				}, {
					"date": "12/09/2019",
					"montant": "9000"
				}, {
					"date": "12/10/2019",
					"montant": "8600"
				}, {
					"date": "12/11/2019",
					"montant": "6400"
				}, {
					"date": "12/12/2019",
					"montant": "8900"
				}, {
					"date": "12/13/2019",
					"montant": "7900"
				}, {
					"date": "12/14/2019",
					"montant": "14000"
				}, {
					"date": "12/15/2019",
					"montant": "16000"
				}, {
					"date": "12/16/2019",
					"montant": "20500"
				}, {
					"date": "12/17/2019",
					"montant": "19000"
				}, {
					"date": "12/18/2019",
					"montant": "8000"
				}, {
					"date": "12/19/2019",
					"montant": "9000"
				}, {
					"date": "12/20/2019",
					"montant": "6700"
				}, {
					"date": "12/21/2019",
					"montant": "8450"
				}, {
					"date": "12/22/2019",
					"montant": "6450"
				}, {
					"date": "12/23/2019",
					"montant": "8900"
				}, {
					"date": "12/24/2019",
					"montant": "14000"
				}, {
					"date": "12/25/2019",
					"montant": "9800"
				}, {
					"date": "12/26/2019",
					"montant": "6900"
				}, {
					"date": "12/27/2019",
					"montant": "8900"
				}, {
					"date": "12/28/2019",
					"montant": "13000"
				}, {
					"date": "12/29/2019",
					"montant": "14000"
				}, {
					"date": "12/30/2019",
					"montant": "31000"
				}, {
					"date": "12/31/2019",
					"montant": "1600"
				}],
				"wharehouse": [{
					"id": "WH0003",
					"address": "Sawmill Cutoff",
					"postalCode": "95420",
					"ville": "Hill Valley",
					"email": "garage@mmvoyages.com",
					"phone": "06 45 85 32 65",
					"latitude": "37.652602",
					"longitude": "-118.957956"
				}]
			}, {
				"id": "B0003",
				"raisonSociale": "Brown Garage",
				"secteurActivite": "Automobile",
				"CA": "1250000",
				"email": "emmett.brown@browngarage.com",
				"phone": "03 45 63 55 16",
				"address": "1640 Riverside Drive",
				"postalCode": "95420",
				"ville": "Hill Valley",
				"pays": "États-Unis",
				"latitude": "37.630395",
				"longitude": "-118.979152",
				"volumeAchatUnite": "320",
				"volumeAchatCumulAnnee": "5 000 000",
				"importancePartner": "Bronze",
				"commercial": "Pierre GREGORUTTI",
				"acheteur": "Jeff BEZOS",
				"volumeAchatCumulInterval": [{
					"date": "12/01/2019",
					"montant": "4500"
				}, {
					"date": "12/02/2019",
					"montant": "3600"
				}, {
					"date": "12/03/2019",
					"montant": "8200"
				}, {
					"date": "12/04/2019",
					"montant": "9100"
				}, {
					"date": "12/05/2019",
					"montant": "10800"
				}, {
					"date": "12/06/2019",
					"montant": "12900"
				}, {
					"date": "12/07/2019",
					"montant": "11000"
				}, {
					"date": "12/08/2019",
					"montant": "9760"
				}, {
					"date": "12/09/2019",
					"montant": "8150"
				}, {
					"date": "12/10/2019",
					"montant": "7640"
				}, {
					"date": "12/11/2019",
					"montant": "6850"
				}, {
					"date": "12/12/2019",
					"montant": "5780"
				}, {
					"date": "12/13/2019",
					"montant": "4600"
				}, {
					"date": "12/14/2019",
					"montant": "3600"
				}, {
					"date": "12/15/2019",
					"montant": "2980"
				}, {
					"date": "12/16/2019",
					"montant": "1750"
				}, {
					"date": "12/17/2019",
					"montant": "2690"
				}, {
					"date": "12/18/2019",
					"montant": "3900"
				}, {
					"date": "12/19/2019",
					"montant": "4780"
				}, {
					"date": "12/20/2019",
					"montant": "5980"
				}, {
					"date": "12/21/2019",
					"montant": "6620"
				}, {
					"date": "12/22/2019",
					"montant": "7950"
				}, {
					"date": "12/23/2019",
					"montant": "8150"
				}, {
					"date": "12/24/2019",
					"montant": "9390"
				}, {
					"date": "12/25/2019",
					"montant": "10780"
				}, {
					"date": "12/26/2019",
					"montant": "11910"
				}, {
					"date": "12/27/2019",
					"montant": "12000"
				}, {
					"date": "12/28/2019",
					"montant": "1600"
				}, {
					"date": "12/29/2019",
					"montant": "4500"
				}, {
					"date": "12/30/2019",
					"montant": "3620"
				}, {
					"date": "12/31/2019",
					"montant": "4900"
				}],
				"wharehouse": [{
					"id": "WH0004",
					"address": "Mammoth Lakes",
					"postalCode": "95420",
					"ville": "Hill Valley",
					"email": "garage@browngarage.com",
					"phone": "06 45 32 95 00",
					"latitude": "37.629213",
					"longitude": "-118.980549"
				}]
			}, {
				"id": "B0004",
				"raisonSociale": "Wayne Enterprises",
				"secteurActivite": "Transport",
				"CA": "15400000",
				"email": "brucewayne@wayneenterprises.com",
				"phone": "04 65 99 55 66",
				"address": "1007 Mountain Drive",
				"postalCode": " 65223",
				"ville": "Gotham",
				"pays": "États-Unis",
				"latitude": "32.678239",
				"longitude": "-117.243085",
				"volumeAchatUnite": "550 000",
				"volumeAchatCumulAnnee": "8 000 000",
				"importancePartner": "Silver",
				"commercial": "Pierre GREGORUTTI",
				"acheteur": "Jeff BEZOS",
				"volumeAchatCumulInterval": [{
					"date": "12/01/2019",
					"montant": "15200"
				}, {
					"date": "12/02/2019",
					"montant": "19000"
				}, {
					"date": "12/03/2019",
					"montant": "26000"
				}, {
					"date": "12/04/2019",
					"montant": "14600"
				}, {
					"date": "12/05/2019",
					"montant": "35200"
				}, {
					"date": "12/06/2019",
					"montant": "67500"
				}, {
					"date": "12/07/2019",
					"montant": "13500"
				}, {
					"date": "12/08/2019",
					"montant": "36000"
				}, {
					"date": "12/09/2019",
					"montant": "49000"
				}, {
					"date": "12/10/2019",
					"montant": "51000"
				}, {
					"date": "12/11/2019",
					"montant": "26000"
				}, {
					"date": "12/12/2019",
					"montant": "80000"
				}, {
					"date": "12/13/2019",
					"montant": "46800"
				}, {
					"date": "12/14/2019",
					"montant": "72500"
				}, {
					"date": "12/15/2019",
					"montant": "46000"
				}, {
					"date": "12/16/2019",
					"montant": "31000"
				}, {
					"date": "12/17/2019",
					"montant": "29000"
				}, {
					"date": "12/18/2019",
					"montant": "17000"
				}, {
					"date": "12/19/2019",
					"montant": "22000"
				}, {
					"date": "12/20/2019",
					"montant": "56000"
				}, {
					"date": "12/21/2019",
					"montant": "34600"
				}, {
					"date": "12/22/2019",
					"montant": "64000"
				}, {
					"date": "12/23/2019",
					"montant": "24500"
				}, {
					"date": "12/24/2019",
					"montant": "19000"
				}, {
					"date": "12/25/2019",
					"montant": "12000"
				}, {
					"date": "12/26/2019",
					"montant": "42150"
				}, {
					"date": "12/27/2019",
					"montant": "59620"
				}, {
					"date": "12/28/2019",
					"montant": "71050"
				}, {
					"date": "12/29/2019",
					"montant": "62500"
				}, {
					"date": "12/30/2019",
					"montant": "15000"
				}, {
					"date": "12/31/2019",
					"montant": "31650"
				}],
				"wharehouse": [{
					"id": "WH0005",
					"address": "Base navale de Gothamn,  03 Mission Point",
					"postalCode": "65223",
					"ville": "Gotham",
					"email": "naval@wayneenterprises.com",
					"phone": "06 98 65 45 22",
					"latitude": "32.688945",
					"longitude": "-117.239860"
				}, {
					"id": "WH0006",
					"address": "Wayne Sea Department, 55 Sanluis Dr",
					"postalCode": "65223",
					"ville": "Gotham",
					"email": "navaldepartment@wayneenterprises.com",
					"phone": "02 65 45 22 00",
					"latitude": "32.680167",
					"longitude": "-117.128127"
				}, {
					"id": "WH0007",
					"address": "Entrepot Wayne, 01 Point Medanos",
					"postalCode": "65223",
					"ville": "Gotham",
					"email": "wharehouse@wayneenterprises.com",
					"phone": "01 23 64 00 45",
					"latitude": "32.758948",
					"longitude": "-117.255568"
				}, {
					"id": "WH0008",
					"address": "R&D Wayne Enterprises, 105 Mission Bay",
					"postalCode": "65223",
					"ville": "Gotham",
					"email": "RD@wayneenterprises.com",
					"phone": "06 45 22 01 33",
					"latitude": "32.784747",
					"longitude": "-117.249535"
				}]
			}, {
				"id": "B0005",
				"raisonSociale": "Manessens Consulting",
				"secteurActivite": "Consulting",
				"CA": "2000000",
				"email": "contact@manessens.com",
				"phone": "02 52 700 200",
				"address": "13 rue de la Rabotière",
				"postalCode": "44800",
				"ville": "Saint-Herblain",
				"pays": "France",
				"latitude": "47.211927",
				"longitude": "-1.620727",
				"volumeAchatUnite": "1 500",
				"volumeAchatCumulAnnee": "800 000",
				"importancePartner": "Gold",
				"commercial": "Julien LUCET",
				"acheteur": "Sébastien ROBERT",
				"volumeAchatCumulInterval": [{
					"date": "12/01/2019",
					"montant": "4500"
				}, {
					"date": "12/02/2019",
					"montant": "4500"
				}, {
					"date": "12/03/2019",
					"montant": "4500"
				}, {
					"date": "12/04/2019",
					"montant": "4500"
				}, {
					"date": "12/05/2019",
					"montant": "4500"
				}, {
					"date": "12/06/2019",
					"montant": "4500"
				}, {
					"date": "12/07/2019",
					"montant": "4500"
				}, {
					"date": "12/08/2019",
					"montant": "4500"
				}, {
					"date": "12/09/2019",
					"montant": "4500"
				}, {
					"date": "12/10/2019",
					"montant": "4500"
				}, {
					"date": "12/11/2019",
					"montant": "4500"
				}, {
					"date": "12/12/2019",
					"montant": "4500"
				}, {
					"date": "12/13/2019",
					"montant": "4500"
				}, {
					"date": "12/14/2019",
					"montant": "4500"
				}, {
					"date": "12/15/2019",
					"montant": "4500"
				}, {
					"date": "12/16/2019",
					"montant": "4500"
				}, {
					"date": "12/17/2019",
					"montant": "4500"
				}, {
					"date": "12/18/2019",
					"montant": "4500"
				}, {
					"date": "12/19/2019",
					"montant": "4500"
				}, {
					"date": "12/20/2019",
					"montant": "4500"
				}, {
					"date": "12/21/2019",
					"montant": "4500"
				}, {
					"date": "12/22/2019",
					"montant": "4500"
				}, {
					"date": "12/23/2019",
					"montant": "4500"
				}, {
					"date": "12/24/2019",
					"montant": "4500"
				}, {
					"date": "12/25/2019",
					"montant": "4500"
				}, {
					"date": "12/26/2019",
					"montant": "4500"
				}, {
					"date": "12/27/2019",
					"montant": "4500"
				}, {
					"date": "12/28/2019",
					"montant": "4500"
				}, {
					"date": "12/29/2019",
					"montant": "4500"
				}, {
					"date": "12/30/2019",
					"montant": "4500"
				}, {
					"date": "12/31/2019",
					"montant": "4500"
				}],
				"wharehouse": [{
					"id": "WH0009",
					"address": "131 avenue Charles de Gaulle",
					"postalCode": "92200",
					"ville": "Neuilly sur seine",
					"email": "contact@manessensparis.com",
					"phone": "01 80 48 26 39",
					"latitude": "48.882212",
					"longitude": "2.2671498"
				}]
			}];

			var oModel = new JSONModel();
			oModel.setData(Business);
			sap.ui.getCore().setModel(oModel);
			this.markers = [];
			this.InforObj = [];
			var me = this;
			this.loadGoogleMaps("https://maps.googleapis.com/maps/api/js?key=AIzaSyBhZdy1pgE2JO-RXSnSHU9ydAp9Y-vHPz8", me.setMapData.bind(me));
			this.cptMarker = 1;
			this.checkboxCA = false;
			this.checkboxSecteurActivite = false;
			this.checkboxPays = false;
			this.checkboxVolumeAchat = false;
			this.checkboxCalendrier = false;
			this.checkboxImportance = false;
			this.checkboxCommercial = false;
			this.checkboxAcheteur = false;
			this.montantTotal = 0;

		},
		loadGoogleMaps: function (scriptUrl, callbackFn) {
			var script = document.createElement('script');
			script.onload = function () {
				callbackFn();
			}
			script.src = scriptUrl;
			document.body.appendChild(script);
		},
		setMapData: function () {
			var myCenter = new google.maps.LatLng(47.211927, -1.620713);
			var mapProp = {
				center: myCenter,
				zoom: 5,
				scrollwheel: true,
				draggable: true,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var map = new google.maps.Map(this.getView().byId("googleMap").getDomRef(), mapProp);
			var marker = new google.maps.Marker({
				position: myCenter
			});
			marker.setMap(map);
		},
		loadData: function () {
			var listMarker = "";
			var oModel = sap.ui.getCore().getModel();
			var data = oModel.getData();
			var oCalendar = this.getView().byId("calendar");
			var dates = oCalendar.getSelectedDates()[0];
			var debut = new Date(dates.getStartDate());
			var fin = new Date(dates.getEndDate());
			var dateDeb = new Date(debut).getTime();
			var dateFin = new Date(fin).getTime();
			var concatenation = " , ";
			for (var i = 0; i < data.length; i++) {
				var latitude = data[i].latitude;
				var longitude = data[i].longitude;
				var raisonSociale = data[i].raisonSociale;
				var secteurActivite = data[i].secteurActivite;
				var chiffreAffaire = data[i].CA;
				var email = data[i].email;
				var phone = data[i].phone;
				var address = data[i].address;
				var postalCode = data[i].postalCode;
				var ville = data[i].ville;
				var volumeAchatUnite = data[i].volumeAchatUnite;
				var volumeAchatCumulAnne = data[i].volumeAchatCumulAnnee;

				var commercial = data[i].commercial;

				var acheteur = data[i].acheteur;

				if (fin.toLocaleDateString() !== "01/01/1970") {
					var montantCA = 0;
					var cpt = (data[i].volumeAchatCumulInterval.length);
					for (var x = 0; x < data[i].volumeAchatCumulInterval.length; x++) {
						var theDate = new Date(data[i].volumeAchatCumulInterval[x].date).getTime();

						if (theDate >= dateDeb && theDate <= dateFin) {
							montantCA += parseInt(data[i].volumeAchatCumulInterval[x].montant);
						}
					}
				}

				var importance = data[i].importancePartner;

				listMarker += latitude + concatenation + longitude + concatenation + raisonSociale + concatenation + secteurActivite +
					concatenation + chiffreAffaire + concatenation + email + concatenation + phone + concatenation + address + concatenation +
					postalCode + concatenation + ville + concatenation + volumeAchatUnite + concatenation + volumeAchatCumulAnne + concatenation +
					commercial + concatenation + acheteur + concatenation + montantCA + concatenation + importance + "/";
			}
			this.filterData(listMarker);
		},
		setMarkers: function (map, markers, content, cpt) {
			var that = this;

			var siteLatLng = new google.maps.LatLng(markers[0], markers[1]);

			var icon = "";
			var sRootPath = jQuery.sap.getModulePath("LAB.GmapUI5.samples");

			if (markers[2] === "Manessens Consulting") {
				icon = sRootPath + "/markerSynvance.png";
			} else {
				icon = "http://maps.google.com/mapfiles/ms/icons/blue-dot.png";
			}

			if (cpt <= 11) {
				switch (that.cptMarker) {
				case 1:
					icon = sRootPath + "/markerDefault.png";
					break;
				case 2:
					icon = sRootPath + "/markerDefaultBlack.png";
					break;
				case 3:
					icon = sRootPath + "/markerDefaultBlue.png";
					break;
				case 4:
					icon = sRootPath + "/markerDefaultBrown.png";
					break;
				case 5:
					icon = sRootPath + "/markerDefaultGreen.png";
					break;
				case 6:
					icon = sRootPath + "/markerDefaultLightBlue.png";
					break;
				case 7:
					icon = sRootPath + "/markerDefaultOrange.png";
					break;
				case 8:
					icon = sRootPath + "/markerDefaultPink.png";
					break;
				case 9:
					icon = sRootPath + "/markerDefaultSynvance.png";
					break;
				case 10:
					icon = sRootPath + "/markerDefaultSynvanceGreen.png";
					break;
				case 11:
					icon = sRootPath + "/markerDefaultYellow.png";
					break;
				default:
					icon = sRootPath + "/markerDefault.png";
					break;
				}
				this.cptMarker++;
			} else if (cpt === 21) {
				console.log(markers[10]);
				if (markers[10] === "Bronze") {

					icon = sRootPath + "/BronzePartner.png";
				} else if (markers[10] === "Silver") {
					icon = sRootPath + "/SilverPartner.png"
				} else if (markers[10] === "Gold") {
					icon = sRootPath + "/GoldPartner.png";
				}

			} else {
				icon = sRootPath + "/markerDefault.png"
			}

			var marker = new google.maps.Marker({
				position: siteLatLng,
				map: map,
				title: markers[2],
				animation: google.maps.Animation.DROP,
				icon: {
					url: icon
				}
			})

			google.maps.event.addListener(marker, "click", function () {
				that.infowindow.setContent(content);
				that.infowindow.open(map, this);
			})
		},

		filterData: function (list) {
			console.log(list);
			var Markers = list.split('/');

			var rangeSlider = this.getView().byId("RangeCA");
			var rangeSliderValueA = 0;
			var rangeSliderValueB = 0;
			if (rangeSlider._liveChangeLastValue) {
				var rangeSliderValue = rangeSlider._liveChangeLastValue;
				rangeSliderValueA = rangeSliderValue[0];
				rangeSliderValueB = rangeSliderValue[1];
			}

			// var partner = partners.split(",");
			// for (var i = 0; i < partners.length - 1; i++) {
			// 	if (this.checkboxCA === true) {

			// 		console.log(partners[i]);
			// 		// if (partner[i][4] >= rangeSliderValueA && partner[i][4] <= rangeSliderValueB) {
			// 		// 	console.log(partner[i]);
			// 		// }
			// 	}

			// }
			var listMarker = "";
			if (this.checkboxCA === true) {
				for (var i = 0; i < Markers.length - 1; i++) {
					var splitMarkers = Markers[i].split(",");
					if (splitMarkers[4] >= rangeSliderValueA && splitMarkers[4] <= rangeSliderValueB) {
						for (var x = 0; x < 16; x++) {
							if (x === 0) {
								listMarker += splitMarkers[x];
							} else if (x > 0 && x < 15) {
								listMarker += "," + splitMarkers[x];
							} else if (x === 15) {
								listMarker += "," + splitMarkers[x] + "/";
							}
						}
					}
				}
			}
			if (this.checkboxSecteurActivite === true) {
				var comboboxSA = this.getView().byId("idComboBox1").getValue();
				var MarkersSA = listMarker.split("/");
				var listMarker2 = "";
				for (var i = 0; i < MarkersSA.length - 1; i++) {
					var splitMarkers2 = MarkersSA[i].split(",");

					if (splitMarkers2[3] === comboboxSA) {
						for (var x = 0; x < 16; x++) {
							if (x === 0) {
								listMarker2 += splitMarkers2[x];
							} else if (x > 0 && x < 15) {
								listMarker2 += "," + splitMarkers2[x];
							} else if (x === 15) {
								listMarker2 += "," + splitMarkers2[x] + "/";
							}
						}
						that.setMarkers(map, splitMarkers, contentString, cpt);
					}

				}
				console.log(listMarker2);
			}

		},
		onUpdate: function () {
			this.checkboxCA = true;
		},
		ChangeActivitySector: function () {
			var comboboxActivity = this.getView().byId("idComboBox1").getValue();
			if (comboboxActivity != "") {
				this.checkboxSecteurActivite = true;
			} else {
				this.checkboxSecteurActivite = false;
			}

		},
		ChangePays: function () {
			var comboboxPays = this.getView().byId("idComboBox2").getValue();
			if (comboboxPays != "") {
				this.checkboxPays = true;
			} else {
				this.checkboxPays = false;
			}

		},
		nbUnite: function () {
			this.checkboxVolumeAchat = true;
		},
		handleCalendarSelect: function () {
			this.checkboxCalendrier = true;
		},
		loadMarker: function () {
			this.checkboxImportance = true;
		},
		ChangeCommercial: function () {
			var comboboxCommercial = this.getView().byId("idComboBox3").getValue();
			if (comboboxCommercial != "") {
				this.checkboxAcheteur = true;
			} else {
				this.checkboxAcheteur = false;
			}

		},
		ChangeAcheteur: function () {
			var comboboxAcheteur = this.getView().byId("idComboBox4").getValue();
			if (comboboxAcheteur != "") {
				this.checkboxAcheteur = true;
			} else {
				this.checkboxAcheteur = false;
			}
			this.loadData();

		}

		// onAfterRendering: function () {
		// 	var me = this;
		// 	this.loadGoogleMaps("https://maps.googleapis.com/maps/api/js?key=AIzaSyBhZdy1pgE2JO-RXSnSHU9ydAp9Y-vHPz8", me.setMapData.bind(me));
		// },

	});
});